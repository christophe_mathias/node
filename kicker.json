{
  "name": "Node.js",
  "description": "Build and test with [Node.js](https://nodejs.org/)",
  "template_path": "templates/gitlab-ci-node.yml",
  "kind": "build",
  "variables": [
    {
      "name": "NPM_CONFIG_REGISTRY",
      "description": "NPM [registry](https://docs.npmjs.com/configuring-your-registry-settings-as-an-npm-enterprise-user)",
      "type": "url",
      "advanced": true
    },
    {
      "name": "NODE_IMAGE",
      "description": "The Docker image used to run Node.js - **set the version required by your project**",
      "default": "node:lts-alpine"
    },
    {
      "name": "NODE_PROJECT_DIR",
      "description": "Node project root directory",
      "default": ".",
      "advanced": true
    },
    {
      "name": "NODE_SOURCE_DIR",
      "description": "Sources directory",
      "default": "src",
      "advanced": true
    },
    {
      "name": "NODE_BUILD_ARGS",
      "description": "NPM [build](https://docs.npmjs.com/cli/build.html) arguments",
      "default": "run build --prod",
      "advanced": true
    },
    {
      "name": "NODE_BUILD_DIR",
      "description": "Variable to define build directory",
      "default": "dist",
      "advanced": true
    },
    {
      "name": "NODE_TEST_ARGS",
      "description": "NPM [test](https://docs.npmjs.com/cli/test.html) arguments",
      "default": "test -- --coverage --bail",
      "advanced": true
    },
    {
      "name": "NODE_UNIT_TEST_REPORT_PATH",
      "description": "The unit test report file path (JUnit format)",
      "default": "reports/unit_test_report.xml",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "npm-lint",
      "name": "npm lint",
      "description": "npm lint analysis",
      "enable_with": "NODE_LINT_ENABLED",
      "variables": [
        {
          "name": "NODE_LINT_ARGS",
          "description": "NPM [run-script](https://docs.npmjs.com/cli/run-script.html) arguments to execute the lint analysis",
          "default": "run lint",
          "advanced": true
        },
        {
          "name": "NODE_LINT_REPORT_PATH",
          "description": "Variable to define lint analysis report path",
          "default": "reports/eslint-report.json",
          "advanced": true
        }
      ]
    },
    {
      "id": "npm-audit",
      "name": "npm audit",
      "description": "npm audit analysis",
      "disable_with": "NODE_AUDIT_DISABLED",
      "variables": [
        {
          "name": "NODE_AUDIT_ARGS",
          "description": "NPM [audit](https://docs.npmjs.com/cli/audit) arguments",
          "default": "--audit-level=low"
        },
        {
          "name": "NODE_AUDIT_JSON_PATH",
          "description": "NPM [audit](https://docs.npmjs.com/cli/audit) JSON report path",
          "default": "reports/npm-audit-report.json",
          "advanced": true
        },
        {
          "name": "NODE_AUDIT_HTML_ARGS",
          "description": "NPM [audit HTML](https://www.npmjs.com/package/npm-audit-html) report generation arguments",
          "default": "--output reports/npm-audit-report.html",
          "advanced": true
        }
      ]
    },
    {
      "id": "njsscan",
      "name": "njsscan",
      "description": "[njsscan](https://github.com/ajinabraham/njsscan) (Static Security Code Scanner) analysis",
      "enable_with": "NODEJSSCAN_ENABLED",
      "variables": [
        {
          "name": "NODEJSSCAN_IMAGE",
          "description": "njsscan image",
          "default": "opensecurity/njsscan:latest",
          "advanced": true
        },
        {
          "name": "NODEJSSCAN_ARGS",
          "description": "njsscan [arguments](https://github.com/ajinabraham/njsscan#command-line-options)",
          "default": "-o reports/nodejsscan-report.txt",
          "advanced": true
        }
      ]
    }
  ]
}
